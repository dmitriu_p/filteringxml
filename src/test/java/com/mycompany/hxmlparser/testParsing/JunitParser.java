/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hxmlparser.testParsing;

import com.mycompany.hxmlparser.model.Rule;
import com.mycompany.hxmlparser.model.RuleComparator;
import com.mycompany.hxmlparser.parser.MySAXHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 *
 * @author Дмитрий
 */
public class JunitParser {
    
    private static Logger logger = Logger.getLogger(JunitParser.class.getName());
    private HashMap<String, Rule> rules=null;
    private HashMap<String, Rule> testRules = new HashMap<>();
    private MySAXHandler myHandler;
    private RuleComparator ruleComparator;
    private Rule rule;
    private Rule rule4;
    private Rule rule5;

    @Before
    public void setUp() {
        rule = new Rule("a", "child", 34);
        Rule rule1 = new Rule("b", "sub", 56);
        Rule rule2 = new Rule("c", "child", 99);
        Rule rule3 = new Rule("d", "root", 45);
        rule4 = new Rule("a", "root", 29);
        rule5 = new Rule("a", "child", 34);
        testRules.put(rule.getName(), rule);
        testRules.put(rule1.getName(), rule1);
        testRules.put(rule2.getName(), rule2);
        testRules.put(rule3.getName(), rule3);
        ruleComparator = new RuleComparator();

        try (InputStream rawXMLStream = this.getClass().getClassLoader().getResourceAsStream("file.xml")){
            FileHandler fh = new FileHandler("%tJunitParser");
            logger.addHandler(fh);
            myHandler = new MySAXHandler();
            rules = myHandler.parserTest(rawXMLStream);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }             
    }
    
    @After
    public  void tearDownToHexStringData() {
        testRules.clear();
        rules.clear();
    }
    
    @Test
    public void testParse() {
        assertEquals(rules.size(), testRules.size());
        for (int i=0; i<rules.size(); i++) {
            assertEquals(rules.get(i), testRules.get(i));
        }
    }
    
    @Test
    public void testComparatorGreaterThan () {    
        int result = ruleComparator.compare(rule, rule4);
        assertTrue("expected to be greater than", result == 1);    
    }
    
    @Test
    public void testComparatorEqual () {    
        int result = ruleComparator.compare(rule, rule5);
        assertTrue("expected to be greater equal", result == 0);   
    }
    
    @Test(expected = NullPointerException.class)
    public void testComparatorExeption() {    
        ruleComparator.compare(rule, null);
        fail("Expected exception was not thrown");
    }
    
    @Test
    public void testSAXExeption(){
        try(InputStream TestXMLStream = this.getClass().getClassLoader().getResourceAsStream("fileTest.xml")) {           
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxparser = factory.newSAXParser();
            MySAXHandler testHandler = new MySAXHandler();
            InputSource inputSource = new InputSource(new InputStreamReader(TestXMLStream));
            saxparser.parse(inputSource, testHandler);
            rules = testHandler.getRules();
            fail("Expected exception was not thrown");
            
        } catch (ParserConfigurationException | SAXException | IOException e) {
            assertTrue(true);            
        }
    }
    
}
