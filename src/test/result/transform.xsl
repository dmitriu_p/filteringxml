<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : transform.xsl
    Created on : 19 июля 2016 г., 22:07
    Author     : Дмитрий
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>transform.xsl</title>
            </head>
            <body>
                <h1>Rule</h1>
                <table border="1">
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Weight</th>
                    </tr>
		 <xsl:for-each select="rules/rule">
                    <tr>
                        <td><xsl:value-of select="@name"/></td>
                        <td><xsl:value-of select="@type"/></td>
                        <td><xsl:value-of select="@weight"/></td>
                    </tr>
                 </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
