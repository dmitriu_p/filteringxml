/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hxmlparser.main;

import com.mycompany.hxmlparser.parser.XMLCreater;
import com.mycompany.hxmlparser.model.Rule;
import com.mycompany.hxmlparser.parser.MySAXHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**This class parses the input XML-file. Filter results and saved it to the output HTML file
 * 
 * @author Дмитрий
 */
public class MainFilterXML {
    
    private static final Logger LOGGER = Logger.getLogger(MainFilterXML.class.getName());
    
    public static void main(String[] args) throws Exception {           
       
        try {
            String inputXMLFilePath;
            FileHandler fh = new FileHandler("%tMainFilterXML");
            LOGGER.addHandler(fh);
            
            if(args.length == 0) {
                inputXMLFilePath = "src/test/result/inputXML.xml";
            } else {
                inputXMLFilePath = args[0];    
            }
            String dataXML = "src/test/result/resultXML.xml";
            String inputXSL = "src/test/result/transform.xsl";
            String outputHTML = "src/test/result/resultHTML.html";
            
            long startTime = System.currentTimeMillis();
            LOGGER.log(Level.INFO, "Start filtering XML");
            
            InputStream rawXMLStream = new FileInputStream(inputXMLFilePath);
            HashMap<String, Rule> rules = parserSAX(rawXMLStream);

            LOGGER.log(Level.INFO, "Create output XML file");
            XMLCreater createXML = new XMLCreater();
            Document rulesDocument = createXML.getXML(rules);

            saveXML(rulesDocument, dataXML);
            saveHTML(rulesDocument, inputXSL, outputHTML);
            long executionTime = System.currentTimeMillis() - startTime;
            LOGGER.log(Level.INFO, "Time took to process " + executionTime + " mill seconds");
            
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }     
   }

    /**called SAX parser for filtering input file
     * 
     * @param rawXMLStream input file transfer
     * @return 
     */
    private static HashMap<String, Rule> parserSAX(InputStream rawXMLStream) {
        HashMap<String, Rule> rules = null;
         
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxparser = factory.newSAXParser();
            MySAXHandler myHandler = new MySAXHandler();
            InputSource inputSource = new InputSource(new InputStreamReader(rawXMLStream));
            saxparser.parse(inputSource, myHandler);
            rules = myHandler.getRules();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return rules;
    }
    
    /**create a transformer object, use the DOM to construct a source object, and use StreamResult to construct a result object
     * 
     * @param doc
     * @param outputPath 
     */
    private static void saveXML(Document doc, String outputPath) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(outputPath).getAbsolutePath());
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.INFO, "Output file XML saved");
   
    }
    
    /**
     * 
     * @param doc
     * @param pathXSLT
     * @param outputHTML 
     */
   private static void saveHTML(Document doc, String pathXSLT, String outputHTML) {
       StreamSource xslStream = new StreamSource(new File(pathXSLT));
       Source resultXMLRules = new DOMSource(doc);
       StreamResult resultHTML = new StreamResult(new File(outputHTML));
       transformXSLT(xslStream, resultXMLRules, resultHTML);
       LOGGER.log(Level.INFO, "Output HTML file saved");        
   }

   /**create a transformer object
    * 
    * @param xslStream
    * @param resultXMLRules
    * @param resultHTML 
    */
    private static void transformXSLT(Source xslStream, Source resultXMLRules, StreamResult resultHTML)
      {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer((Source) xslStream);
            transformer.transform(resultXMLRules, resultHTML);
            
        } catch ( TransformerException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }  
    }
   
}
    

