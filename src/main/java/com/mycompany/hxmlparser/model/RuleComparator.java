
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hxmlparser.model;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Дмитрий
 */
public class RuleComparator implements Comparator<Rule> {

    private static Logger logger = Logger.getLogger(RuleComparator.class.getName());
    
    @Override
    public int compare(Rule o1, Rule o2) {
        
        //if different types have the same priority
        Integer typePriority1 = getTypePriotiry(o2.getType());
        Integer typePriority2 = getTypePriotiry(o2.getType());
        if(typePriority1.equals(typePriority2)){
            return Integer.compare(o1.getWeight(), o2.getWeight());
        } else {
            return typePriority1.compareTo(typePriority2);
        }       
    }
    
    /**establish the order the type of the advantages
     * 
     * @param type
     * @return 
     */
    private  int getTypePriotiry (String type) {  

        HashMap<String, Integer> weights = new HashMap<String, Integer>();
        weights.put("child", 1);
        weights.put("sub", 2);
        weights.put("root", 3);
        
         if(!weights.containsKey(type)){
             logger.log(Level.WARNING, "Type is not found");
         }
         return weights.get(type);
    }
    
    
}
