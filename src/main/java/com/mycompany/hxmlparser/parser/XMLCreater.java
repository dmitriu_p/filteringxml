/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hxmlparser.parser;

import com.mycompany.hxmlparser.main.MainFilterXML;
import com.mycompany.hxmlparser.model.Rule;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**Creating the Document on the basis of filtering results input file
 * 
 * @author Дмитрий
 */
public class XMLCreater {
    
    private Logger logger = Logger.getLogger(MainFilterXML.class.getName());
    
    /**Returns an Document object that can then be used for output result
     * 
     * @param rules result filtering xml
     * @return the document depends on the collection
     */
    public Document getXML(HashMap<String, Rule> rules) {
        Document doc = null;
        try {
            FileHandler fh = new FileHandler("%tXMLCreater");
            logger.addHandler(fh);
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = (Document) docBuilder.newDocument();
            Element rootElement = doc.createElement("rules");
            doc.appendChild(rootElement);

            for(Entry<String, Rule> entry : rules.entrySet()) {
                String key = entry.getKey();
                Rule rule = entry.getValue();
                String weight = String.valueOf(rule.getWeight());
                rootElement.appendChild(getRule(doc, rule.getName(), rule.getType(), weight));
            }   
            return doc;	
	  } catch (IOException | ParserConfigurationException ex) {               
                logger.log(Level.SEVERE, null, ex);
                
	  } finally {
            doc = null;          
        }
        return doc;   
    }

    /**creating an element based on the Rule object
     * 
     * @param doc
     * @param name
     * @param type
     * @param weight
     * @return 
     */
    private Node getRule(Document doc, String name, String type, String weight) {
        Element rule = doc.createElement("rule");
        rule.setAttribute("name", name);
        rule.setAttribute("type", type);
        rule.setAttribute("weight", weight);
        return rule;
    }
    
}
