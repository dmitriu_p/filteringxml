/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hxmlparser.parser;

import com.mycompany.hxmlparser.model.Rule;
import com.mycompany.hxmlparser.model.RuleComparator;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**Contains the collections of Rule object as field with a getter method only.
 * Rule field used to create Rule object and once all the fields are set, 
 * add it to the collection.
 *
 * @author Дмитрий
 */
public class MySAXHandler extends DefaultHandler {

    private static Logger logger = Logger.getLogger(MySAXHandler.class.getName());
    private HashMap<String, Rule> rules = null;
    private Rule currentRule = null;
    RuleComparator ruleComparator = new RuleComparator();

    /**
     * 
     * @return 
     */
    public HashMap<String, Rule> getRules() {
        return rules;
    }

    @Override
    public void startElement( String namespaceURI, String localName, String qName, Attributes attributes)
            throws SAXException {
        if (qName.equalsIgnoreCase("Rule")) {
            currentRule = new Rule();
            currentRule.setName(attributes.getValue("name"));
            currentRule.setType(attributes.getValue("type"));
            currentRule.setWeight(Integer.parseInt(attributes.getValue("weight")));
        if (rules == null)
            rules = new HashMap<String, Rule>();
    } 
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName)
            throws SAXException {

        if (qName.equalsIgnoreCase("Rule")) {
            String key = currentRule.getName();
            if (rules.containsKey(currentRule.getName())) {
                // replace rule if the new one has bigger priority
                Rule oldRule = rules.get(currentRule.getName());
                if(ruleComparator.compare(currentRule, oldRule) == 1){
                    rules.put(key, currentRule);
                }
                } else {
                    rules.put(key, currentRule);
                }
        }
    }
    
    /**
     * called for testing SAX parser in JUnit
     * @param rawXMLStream
     * @return 
     */ 
    public HashMap<String, Rule> parserTest(InputStream rawXMLStream ) {
        HashMap<String, Rule> rules = null;
         
        try {
            FileHandler fh = new FileHandler("%tMySAXHandler");
            logger.addHandler(fh);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxparser = factory.newSAXParser();
            MySAXHandler myHandler = new MySAXHandler();
            InputSource inputSource = new InputSource(new InputStreamReader(rawXMLStream));
            saxparser.parse(inputSource, myHandler);
            rules = myHandler.getRules();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MySAXHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rules;
    }

}
